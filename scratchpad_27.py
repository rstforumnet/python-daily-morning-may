# import paramiko

# HOST = "172.105.37.164"
# USERNAME = "root"
# PASSWORD = ""

# ssh = paramiko.SSHClient()
# ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
# ssh.connect(hostname=HOST, username=USERNAME, password=PASSWORD)

# with open("commands.txt") as file:
#     commands = file.readlines()

# for command in commands:
#     stdin, stdout, stderr = ssh.exec_command(command)
#     print(stdout.read().decode())

# ssh.close()

import pyautogui
import os
from time import sleep

filename = "example.txt"
content = "This is some dummy content"

pyautogui.press("winleft")
pyautogui.write("notepad")
pyautogui.press("enter")
sleep(2)

pyautogui.write(content)
sleep(3)

pyautogui.hotkey("ctrl", "s")
sleep(2)
pyautogui.write(os.path.join(os.path.expanduser("~"), "Downloads", filename))
pyautogui.press("enter")
sleep(1)

pyautogui.hotkey("alt", "f4")
sleep(1)

pyautogui.press("n")
print(
    f"Successfully created file {filename} in the Downloads folder and closed the application."
)
