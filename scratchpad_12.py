# # def hello:
# #     return "hello world"


# # def print_in_color(text, color_name):
# #     colors = ("red", "yellow", "blue", "black", "white", "green", "orange")

# #     if type(text) != str:
# #         raise Exception("Text should be a string")

# #     if type(color_name) != str:
# #         raise TypeError("Color name should be a string")

# #     if color_name not in colors:
# #         raise ValueError("Sorry, that color is not supported")

# #     print(f"{text} ---- in {color_name}")


# # try:
# #     print_in_color("Hello World", 0)
# # except:
# #     print("Something went wrong")

# # try:
# #     print_in_color("Hello World", "purple")
# # except TypeError:
# #     print("Something went wrong")


# # try:
# #     print_in_color("Hello World", "purple")
# # except TypeError:
# #     print("Incorrect type")
# # except ValueError:
# #     print("Not the right value")

# # try:
# #     print_in_color("Hello World", 0)
# # except (TypeError, ValueError):
# #     print("Type or value is incorrect")

# # try:
# #     print_in_color(0, "purple")
# # except (TypeError, ValueError) as err:
# #     print(err)

# # try:
# #     print_in_color(0, "purple")
# # except Exception as err:
# #     print(err)


# def print_in_color(text, color_name):
#     colors = ("red", "yellow", "blue", "green", "cyan")

#     if type(text) != str:
#         raise Exception("Text should be a string")

#     if type(color_name) != str:
#         raise TypeError("Color name should be a string")

#     if color_name not in colors:
#         raise ValueError("Sorry, that color is not supported")

#     if color_name == "red":
#         return f"\033[0;31m{text}\033[0m"
#     elif color_name == "yellow":
#         return f"\033[1;33m{text}\033[0m"
#     elif color_name == "blue":
#         return f"\033[0;34m{text}\033[0m"
#     elif color_name == "green":
#         return f"\033[0;32m{text}\033[0m"
#     elif color_name == "cyan":
#         return f"\033[0;36m{text}\033[0m"


# # try:
# #     result_to_be_printed = print_in_color("Hello World", "red")
# # except Exception as err:
# #     print(err)
# # else:
# #     print(result_to_be_printed)

# try:
#     result_to_be_printed = print_in_color("Hello World", "zinc")
# except Exception as err:
#     print(err)
# else:
#     print(result_to_be_printed)
# finally:
#     print("THIS WILL ALWAYS RUN")

# # print_in_color("Hello World", 0)

# print()
# print()
# print("THIS RUNS AFTER EVERYTHING ELSE.")


def greet():
    return "Hello World!"


def add(a, b):
    return a + b


# print("This is coming from the scratchpad_12 module")

if __name__ == "__main__":
    print("FROM scratchpad_12", __name__)
