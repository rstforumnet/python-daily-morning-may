import requests
from bs4 import BeautifulSoup
from csv import writer

response = requests.get("https://arstechnica.com/")
# print(response.text)

soup = BeautifulSoup(response.text, "html.parser")

articles = soup.find_all(class_="article")

with open("articles.csv", "w", newline="") as file:
    csv_writer = writer(file)
    csv_writer.writerow(["Title", "Excerpt", "Author", "Published Date", "URL"])

    for article in articles:
        title = article.find("h2").get_text()
        excerpt = article.find(class_="excerpt").get_text()
        author = article.find("span").get_text()
        date = article.find("time").get_text()
        url = article.find("a")["href"]

        csv_writer.writerow([title, excerpt, author, date, url])
