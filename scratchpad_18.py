# # # def my_for(iterable):
# # #     iterator = iter(iterable)

# # #     while True:
# # #         try:
# # #             print(next(iterator))
# # #         except StopIteration:
# # #             break


# # # my_for("hello world")


# # # for char = next(x) in x = iter("hello"):
# # #     print(char)


# class Counter:
#     def __init__(self, start, end, step=1):
#         self.start = start
#         self.end = end
#         self.step = step

#     def __repr__(self):
#         return f"Counter({self.start}, {self.end})"

#     def __iter__(self):
#         return self

#     def __next__(self):
#         if self.start < self.end:
#             num = self.start
#             self.start += self.step
#             return num
#         else:
#             raise StopIteration


# # # r = range(0, 10)

# # # for num in r:
# # #     print(num)

# # c = Counter(0, 10, 2)

# # for num in c:
# #     print(num)


# def counter(start, end, step=1):
#     while start < end:
#         yield start
#         start += step


# c = counter(0, 10, 2)

# for num in c:
#     print(num)


# def fib_list(max):
#     nums = []
#     a, b = 0, 1
#     while len(nums) < max:
#         nums.append(a)
#         a, b = b, a + b
#     return nums


# a = fib_list(100000000000000000000000000000000000000000)
# for num in a:
#     print(num)


# def fib_gen(max):
#     a, b = 0, 1
#     count = 0
#     while count < max:
#         a, b = b, a + b
#         yield a
#         count += 1


# b = fib_gen(100000000000000000000000000000000000000000)
# for num in b:
#     print(num)


# nums1 = [num for num in range(10000)]
# nums2 = (num for num in range(10000))

# print(nums1)
# print(nums2)

import time

list_start_time = time.time()
print(sum([num for num in range(100000000)]))
list_stop = time.time() - list_start_time


gen_start_time = time.time()
print(sum(num for num in range(100000000)))
gen_stop = time.time() - gen_start_time


print(f"List Comp took: {list_stop}")
print(f"Gen took: {gen_stop}")
