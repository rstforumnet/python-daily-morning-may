# DATA
# FUNCTIONS

# s1 = {"title": "", "track_length": 4.32, "album": "", "artist": []}

# playlist = [
#     {"title": "", "track_length": 4.32, "album": "", "artist": []},
#     {"title": "", "track_length": 4.32, "album": "", "artist": []},
# ]


# pl = {
#     "user_id": 1,
#     "created_at": "20-3-2024",
#     "songs": [
#         {"title": "", "track_length": 4.32, "album": "", "artist": []},
#         {"title": "", "track_length": 4.32, "album": "", "artist": []},
#     ],
# }


# def add_song_to_playlist(playlist, song):
#     playlist["songs"].append(song)


# add_song_to_playlist(pl, s1)


def create_song(title, track_length, album_name, artist_list=[]):
    return {
        "title": title,
        "track_length": track_length,
        "album": album_name,
        "artists": artist_list,
    }


def change_song_title(song, new_title):
    song["title"] = new_title


song1 = create_song("Song #1", 3.42, "Some album", ["John Doe", "Jane Doe"])

change_song_title(song1, "Hello world")

print(song1["title"])


class Song:
    def __init__(self, title, track_length, album_name, artist_list=[]):
        self.title = title
        self.track_length = track_length
        self.album = album_name
        self.artists = artist_list

    def edit_title(self, new_title):
        self.title = new_title


song2 = Song("Song #2", 5.32, "Hello world!", ["Jack Smith"])
song2.edit_title("Good bye world")
print(song2.title)
# print(song2.album)

print(type(song1))
print(type(song2))
