# import sqlite3

# conn = sqlite3.connect("my_python_users.db")
# cursor = conn.cursor()

# # cursor.execute(
# #     """CREATE TABLE users (id INTEGER PRIMARY KEY, name TEXT, email TEXT, age INTEGER);"""
# # )

# # user1_data = ("John Doe", "johndoe@hey.com", 30)
# # cursor.execute("INSERT INTO users (name, email, age) VALUES (?, ?, ?)", user1_data)

# cursor.execute("SELECT * FROM users;")
# all_users = cursor.fetchall()

# print(all_users)

# # conn.commit()
# conn.close()

import pymongo
from pprint import pprint

client = pymongo.MongoClient()
db = client["python_users"]
users = db["users"]

# users_data = [
#     {"name": "John Dee", "email": "johndee@gmail.com", "age": 30},
#     {"name": "Jane Dee", "email": "janedee@hey.com", "age": 40},
# ]

# users.insert_many(users_data)

# all_users = users.find()

# pprint(list(all_users))
