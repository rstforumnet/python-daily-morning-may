import random
from datetime import datetime


def random_text_generator(length=10):
    chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    text = ""
    for _ in range(length):
        text += random.choice(chars)
    return text


class Company:
    def __init__(
        self,
        name,
        address,
        phone_nos,
        email,
        company_registration_code,
    ):
        self.name = name
        self.address = address
        self.phone_nos = phone_nos
        self.email = email
        self.company_registration_code = company_registration_code
        self.employees = []

    def __repr__(self):
        return self.name

    def create_employee(
        self,
        first_name,
        middle_name,
        last_name,
        address,
        aadhar_card_no,
        pan_card_no,
        phone,
        email,
    ):
        if not all(
            [
                first_name,
                middle_name,
                last_name,
                address,
                aadhar_card_no,
                pan_card_no,
                phone,
                email,
            ]
        ):
            raise ValueError("All fields are required to create an employee")

        new_employee = Employee(
            first_name,
            middle_name,
            last_name,
            address,
            aadhar_card_no,
            pan_card_no,
            phone,
            email,
        )
        self.employees.append(new_employee)
        return new_employee

    def find_employee(self, employee_id):
        for emp in self.employees:
            if emp.employee_id == employee_id:
                return emp
        raise ValueError("Employee not found")

    def filter_employees(self, name):
        filtered = [
            employee for employee in self.employees if name in employee.get_full_name()
        ]
        if not filtered:
            raise ValueError("No employee found with this name")
        return filtered


class Employee:
    def __init__(
        self,
        first_name,
        middle_name,
        last_name,
        address,
        aadhar_card_no,
        pan_card_no,
        phone,
        email,
    ):
        self.first_name = first_name
        self.middle_name = middle_name
        self.last_name = last_name
        self.address = address
        self.aadhar_card_no = aadhar_card_no
        self.pan_card_no = pan_card_no
        self.phone = phone
        self.email = email
        self.employee_id = random_text_generator()
        self.attendance = []

    def __repr__(self):
        return f"{self.first_name} {self.middle_name} {self.last_name} ({self.employee_id})"

    def get_full_name(self):
        return f"{self.first_name} {self.middle_name} {self.last_name}"

    def get_details(self):
        return {
            "first_name": self.first_name,
            "middle_name": self.middle_name,
            "last_name": self.last_name,
            "address": self.address,
            "aadhar_card_no": self.aadhar_card_no,
            "pan_card_no": self.pan_card_no,
            "email": self.email,
            "phone": self.phone,
            "employee_id": self.employee_id,
        }

    def edit_details(
        self,
        first_name=None,
        middle_name=None,
        last_name=None,
        address=None,
        phone=None,
        email=None,
        aadhar_card_no=None,
        pan_card_no=None,
    ):
        if first_name:
            self.first_name = first_name
        if middle_name:
            self.middle_name = middle_name
        if last_name:
            self.last_name = last_name
        if address:
            self.address = address
        if phone:
            self.phone = phone
        if email:
            self.email = email
        if aadhar_card_no:
            self.aadhar_card_no = aadhar_card_no
        if pan_card_no:
            self.pan_card_no = pan_card_no

    def punch_in(self):
        self.attendance.append({"date": datetime.now(), "punch": "in"})

    def punch_out(self):
        self.attendance.append({"date": datetime.now(), "punch": "out"})


# acme = Company(
#     "Acme Corp",
#     "Some street, whatever",
#     ["02209876542", "+91987867643"],
#     "support@acme.com",
#     "ABC123",
# )

# john = acme.create_employee(
#     "John",
#     "Jack",
#     "Doe",
#     "Some street",
#     "ABC12345553",
#     "UHJI862322",
#     "+919829987654",
#     "johndoe@gmail.com",
# )
# john.punch_in()
# print(john.attendance)
# print(john.get_details())


# Bank
# Customer
# Employee

# Bank (bank.py)
# Attributes: name, initials, address, branch, ifsc_code, micr_code, phone_nos, employees: list[Employee], customers: List[Customer]
# Methods: get_details, edit_details(), get_customers_list(), find_customer_by_account_no(), find_customer_by_id(), filter_customer_by_name(), find_customer_by_email(), find_customer_by_phone(), delete_customer(), export_customers_as_csv()

# Customer (customer.py)
# Attributes: first_name (2 chars), middle_name (2 char), last_name (2 char), phone, email, date_of_birth, aadhar_card_no, pan_card_no, customer_id, account_no, balance (5000), account_type
# Methods: get_full_name(), get_details(), edit_details(), get_balance(), deposit(), withdraw()

# Employee (employee.py)
# Attributes: first_name, middle_name, last_name, phone, email, date_of_birth, aadhar_card_no, pan_card_no, employee_id, job_title, salary, attendance
# Methods: get_full_name(), get_details(), edit_details(), punch_in(), punch_out()


# class Helper:
#     @classmethod
#     def is_valid_email(cls): ...


# class User:
#     def __init__(self, name, email, phone):
#         if not is_valid_email(email):
#             raise ValueError("Please enter a valid email")

#         self.name = name
#         self.email = email


# day/month/year (dd/mm/YYYY)
