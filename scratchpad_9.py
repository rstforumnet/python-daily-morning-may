# from random import random


# def flip_coin():
#     random_num = random()
#     if random_num > 0.5:
#         print("HEADS")
#     else:
#         print("TAILS")


# flip_coin()
# flip_coin()
# flip_coin()


# def say_hello():
#     print("Hello World")
#     print("Hello World")
#     print("Hello World")
#     print("Hello World")
#     print("Hello World")
#     print("Hello World")


# say_hello()
# say_hello()


# def say_hello():
#     print("Hello again")
#     return "Hello World"


# # result = say_hello()
# # print(result)

# print(say_hello())


# def add(x, y):  # params
#     return x + y


# print(add(10, 20))


# def say_hello(name, message):
#     return f"{message}, {name}"


# result = say_hello("Jane", "Hello")
# print(result)
# result = say_hello("John", "Hi")
# print(result)


# def sum_odd_nums(nums):
#     total = 0
#     for num in nums:
#         if num % 2 == 1:
#             total += num
#     return total


# print(sum_odd_nums([1, 2, 3, 4, 5, 9]))


# def is_odd_num(num):
#     if num % 2 == 1:
#         return True
#     return False


# print(is_odd_num(9))


# def add(x=0, y=0):
#     return x + y


# print(add())


# def add(x=0, y=0):
#     return x + y


# print(add)

# print(add(10, 20))
# print(add("hello", "world"))


# def print_full_name(first_name, last_name, age):
#     return f"Hello, my name is {first_name} {last_name} and I am {age} years old"


# # print(print_full_name("John", "Doe", 20))
# # print(print_full_name(20, "John", "Doe"))

# print(print_full_name(age=20, last_name="Doe", first_name="John"))


# full_name = "John Doe"


def say_hello():
    full_name = "Jane Smith"
    print(full_name)


say_hello()

print(full_name)
