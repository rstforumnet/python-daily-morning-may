# # import random as rd

# # # print(random.randint(0, 10))
# # # print(random.random())


# # def random():
# #     return "Hello world"


# # print(random())
# # print(rd.randint(0, 10))

# # from random import randint as ri, random as r, shuffle

# # # from random import *


# # print(r())
# # print(ri(10, 100))

# # a = ["hello", "world", "test", "something"]
# # print(shuffle(a))
# # print(a)


# # import scratchpad_12
# from scratchpad_12 import add, greet

# # print(scratchpad_12.add(10, 20))
# # print(scratchpad_12.greet())

# print(add(10, 20))
# print(greet())

# # print("TEST1: Hello World. This is coming from the scratchpad_13 module")

# print("FROM scratchpad_13", __name__)


# def capitalize_sentence(sentence):
#     words = sentence.split()
#     cap_words = []
#     for word in words:
#         cap_words.append(word[0].upper() + word[1:])
#     return " ".join(cap_words)


# print(capitalize_sentence("hello world this is something"))


def is_palindrome(word):
    return word == word[::-1]


print(is_palindrome("hello"))
