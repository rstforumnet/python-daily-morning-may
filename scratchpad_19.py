# # def add(a, b):
# #     return a + b


# # # print(add)
# # hello = add

# # # print(add(10, 20))
# # print(hello(10, 20))


# def math(a, b, fn):
#     return fn(a, b)


# def sub(a, b):
#     return a - b


# print(math(10, 20, sub))


# def sum(n, func):
#     total = 0
#     for num in range(1, n + 1):
#         total += func(num)
#     return total


# def square(n):
#     return n * n


# def cube(n):
#     return n * n * n


# print(sum(3, square))
# print(sum(3, cube))


# import random


# def greet(person):
#     def get_mood():
#         mood = ["Hey", "What!", "What the heck do you want!", "Get lost!"]
#         msg = random.choice(mood)
#         return msg

#     result = f"{get_mood()} {person}"
#     return result


# print(greet("John"))


# def make_greet_func(person):
#     def make_message():
#         msg = random.choice(["Hey", "What!", "What the heck do you want!", "Get lost!"])
#         return f"{msg} {person}"

#     return make_message


# result = make_greet_func("John")
# result2 = make_greet_func("Jane")

# print(result())
# print(result2())


# def stars(fn):
#     def wrapper():
#         print("*" * 10)
#         fn()
#         print("*" * 10)

#     return wrapper


# def say_hello():
#     print("Hello World")


# result = stars(say_hello)

# # say_hello()
# result()

# from typing import Callable


# def math(
#     a: int | float,
#     b: int | float,
#     fn: Callable[[int | float, int | float], int | float],
# ) -> int | float:
#     return fn(a, b)


# def math(a, b, fn):
#     return fn(a, b)


# def add(a, b):
#     return a + b


# def hello_world(a, b):
#     return a - b


# print(math(10, 20, hello_world))


# print(add(10, 20))

from random import choice


# # Function Factory
# def make_greet(person_name):
#     def make_message():
#         greetings = ["Hey", "Hi", "Hello", "Good morning"]
#         message = choice(greetings)
#         return f"{message} {person_name}"

#     return make_message


# result = make_greet("John")
# result2 = make_greet("Jane")
# result()


def stars(fn):
    def wrapper():
        print("-*-" * 10)
        fn()
        print("-*-" * 10)

    return wrapper


@stars
def say_hello():
    print("Hello world this is something")


# say_hello = stars(say_hello)
say_hello()
