# total = 0


# def dummy():
#     global total
#     total += 1
#     print(total)


# dummy()

# print(total)


# def outer():
#     count = 0

#     def inner():
#         nonlocal count
#         count += 1
#         print(count)

#     inner()


# outer()


# def greet(first_name: str, message: str) -> str:
#     """
#     This function will accept name a message and greet that user.
#     """
#     return f"{message} {first_name}"


# # print(greet("John", "Hello, my name is"))

# print(greet.__doc__)

# print([].append.__doc__)


# "hello world this is something"
# => "Hello World This Is Something"

# We need to capitalize the first letter of every word in the sentence. We cannot use title and capitalize.


# def capitalize_str(sentence):
#     words = sentence.split()
#     upper_words = []
#     for word in words:
#         upper_words.append(word[0].upper() + word[1:])
#     return " ".join(upper_words)


# print(capitalize_str("hello world this is something"))


# kayak


# def is_palindrome(word):
#     return word == word[::-1]


# print(is_palindrome("kayak"))
