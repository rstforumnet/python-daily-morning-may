# # def add(a, b, c=0, d=0):
# #     return a + b + c + d


# # print(add(10, 20, 30, 40))


# # def add(*nums):
# #     total = 0
# #     for num in nums:
# #         total += num
# #     return total


# # print(add(10, 20, 30, 40, 50, 60, 70, 80, 90))


# # def add(a, b, c, *nums):
# #     print(a)
# #     print(b)
# #     print(c)
# #     print(nums)


# # add(10, 20, 30, 40, 50, 60, 70, 80, 90)


# # def profile(a, b, *args, role="admin", **data):
# #     print(a)
# #     print(b)
# #     print(args)
# #     print(role)
# #     print(data)


# # profile(
# #     190,
# #     20,
# #     30,
# #     40,
# #     50,
# #     60,
# #     first_name="John",
# #     last_name="Doe",
# #     age=20,
# #     hello="world",
# #     role="moderator",
# # )


# # def add(*nums):
# #     total = 0
# #     for num in nums:
# #         total += num
# #     return total


# # data = [10, 20, 30, 40, 100]

# # print(add(*data))


# def profile(first_name, last_name, age):
#     print(f"Hello my name is {first_name} {last_name} and I am {age} years old.")


# data = {"first_name": "Jane", "last_name": "Smith", "age": 30}


# profile("John", "Doe", 20)
# profile(first_name="Jane", last_name="Roe", age=50)
# profile(**data)


# def add1(a, b):
#     return a + b


# add2 = lambda a, b: a + b

# square = lambda x: x**2

# greet = lambda: "hello world"

# print(add2(10, 20))
# print(square(10))
# print(greet())


# def math(a, b, fn):
#     return fn(a, b)


# def sub(a, b):
#     return a - b


# print(math(50, 30, sub))

# print(math(50, 30, lambda a, b: a * b))
# print(math(50, 30, lambda a, b: a / b))
# print(math(50, 30, lambda a, b: a + b))

# nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

# doubles = []

# for num in nums:
#     doubles.append(num * 2)

# doubles = [num * 2 for num in nums]

# doubles = map(lambda x: x * 2, nums)
# print(list(doubles))


nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

# evens = [num for num in nums if num % 2 == 0]

# evens = []
# for num in nums:
#     if num % 2 == 0:
#         evens.append(num)

# evens = filter(lambda num: num % 2 == 0, nums)

# print(list(evens))


# names = ["John", "Jack", "James", "Desmond", "Charlie", "Jacob"]

# result = map(
#     lambda name: f"The one who wins is {name}",
#     filter(lambda name: len(name) <= 4, names),
# )

# print(list(result))
