# # names = ["john", "jack", "james", "jill", "jane"]

# # for name in names:
# #     print(name)

# data = {
#     "description": "Some description",
#     "name": "hello world",
#     "brand": "Apple",
#     "in_stock": 510,
#     "price": 100000,
#     "image_url": "https://url...",
# }

# # for value in data.values():
# #     print(value)


# # for key in data.keys():
# #     print(key, data[key])

# # for key in data:
# #     print(key, data[key])


# # for item in data.items():
# #     print(item[0], item[1])


# for key, value in data.items():
#     print(key, value)


user1 = {"first_name": "John", "last_name": "Doe", "email": "john@gmail.com"}

user2 = {"age": 20}
