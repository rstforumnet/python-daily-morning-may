# langs = ["Python", "JavaScript", "Rust", "C++", "ReScript", "TypeScript", "Perl"]

# # upper_langs = [lang.upper() for lang in langs]
# # upper_langs = [lang[0] for lang in langs]

# # upper_langs = []

# # for lang in langs:
# #     upper_langs.append(lang.upper())

# # print(upper_langs)


# # nums = [1, 2, 234, 235, 345, 34, 52]

# # evens = []

# # doubles = [num * 2 for num in nums]
# # # doubles = []

# # # for num in nums:
# # #     doubles.append(num * 2)


# # print(doubles)


# nums = [1, 2, 234, 235, 345, 34, 52]

# results = [num * 2 if num % 2 == 0 else num / 2 for num in nums]

# # evens = [num for num in nums if num % 2 == 0]

# # evens = []

# # for num in nums:
# #     if num % 2 == 0:
# #         evens.append(num)

# print(results)


nums = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

# for row in nums:
#     for col in row:
#         print(col)

total = 0

for row in nums:
    for col in row:
        total += col

print(total)
