# import pickle
# import json
# import jsonpickle


# class User:
#     def __init__(self, first_name, last_name, age):
#         self.first_name = first_name
#         self.last_name = last_name
#         self.age = age

#     def __repr__(self):
#         return self.first_name

#     def greet(self):
#         return "Hello World"


# # john = User("John", "Doe", 20)
# # jane = User("Jane", "Smith", 37)

# # print(json.dumps(john.__dict__))

# # with open("users.json", "w") as file:
# #     data = jsonpickle.encode((john, jane))
# #     file.write(data)

# with open("users.json") as file:
#     contents = file.read()
#     data = jsonpickle.decode(contents)
#     print(data[0].greet())


# # with open("users.pickle", "wb") as file:
# #     pickle.dump([john, jane], file)


# # with open("users.pickle", "rb") as file:
# #     users = pickle.load(file)
# #     print(users[0].greet())
