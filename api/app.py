from flask import Flask, request, jsonify
from pymongo import MongoClient
from bson import ObjectId
from twilio.rest import Client
from random import randint

app = Flask(__name__)
client = MongoClient()
db = client["python_users_db"]
coll = db["users"]

# Twilio Credentials
account_sid = ""
auth_token = ""
twilio_phone_number = ""

twilio_client = Client(account_sid, auth_token)


def generate_otp():
    return str(randint(1000, 1999))


@app.route("/", methods=["GET"])
def home():
    return {"message": "API is running."}


# Create a new user document
@app.route("/api/users", methods=["POST"])
def create_user():
    data = request.json
    otp = generate_otp()

    message = f"Your OTP for user registration is: {otp}"
    twilio_client.messages.create(
        to=data["phone"], from_=twilio_phone_number, body=message
    )
    data["otp"] = otp

    result = coll.insert_one(data)
    return (
        {
            "success": True,
            "message": "User created successfully",
            "id": str(result.inserted_id),
        },
        201,
    )


# Verify the OTP and update the user record
@app.route("/api/users/verify_otp/<id>", methods=["POST"])
def verify_otp(id):
    data = request.json
    user = coll.find_one({"_id": ObjectId(id)})
    if user is None:
        return ({"message", "User not found"}, 404)
    if user.get("verified", False):
        return ({"message": "User already verified"}, 400)
    if user["otp"] != data["otp"]:
        return ({"message": "Invalid OTP"}, 400)

    result = coll.update_one(
        {"_id": ObjectId(id)}, {"$set": {"verified": True}, "$unset": {"otp": ""}}
    )
    if result.modified_count == 0:
        return ({"message": "User already verified"}, 400)
    return ({"message": "User verified"}, 200)


# Get all user documents
@app.route("/api/users", methods=["GET"])
def get_all_users():
    data = list(coll.find())
    for user in data:
        user["_id"] = str(user["_id"])
    return (jsonify(data), 200)


# Get a single user document by ID
@app.route("/api/users/<id>", methods=["GET"])
def get_user(id):
    data = coll.find_one({"_id": ObjectId(id)})
    if data is None:
        return ({"success": False, "message": "User not found"}, 404)
    data["_id"] = str(data["_id"])
    return jsonify(data)


# Update a user document by ID
@app.route("/api/users/<id>", methods=["PUT"])
def update_user(id):
    data = request.json
    result = coll.update_one({"_id": ObjectId(id)}, {"$set": data})
    if result.modified_count == 0:
        return ({"success": False, "message": "User not found"}, 404)
    return {"success": True, "message": "User updated"}


# Delete a user document by ID
@app.route("/api/users/<id>", methods=["DELETE"])
def delete_user(id):
    result = coll.delete_one({"_id": ObjectId(id)})
    if result.deleted_count == 0:
        return ({"success": False, "message": "User not found"}, 404)
    return {"success": True, "message": "User deleted"}


if __name__ == "__main__":
    app.run(debug=True, port=8000)
