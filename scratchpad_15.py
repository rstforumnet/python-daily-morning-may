# # def sqaure(width, height):
# #     return {"height": height, "width": width}


# class Square:
#     def __init__(self, height, width):
#         self.height = height
#         self.width = width


# sq2 = {"width": 30, "height": 40}
# sq1 = Square(20, 40)

# print(sq1)
# print(sq2)


class User:
    total = 0

    def __init__(self, first_name, last_name, age, email, phone):
        self.first_name = first_name
        self.last_name = last_name
        self._age = age
        self.email = email
        self.phone = phone
        self.city = "Mumbai"
        User.total += 1

    def __repr__(self):
        return f"Full Name:\n{self.first_name} {self.last_name}\n"

    def greet(self):
        return f"Hello, my name is {self.first_name} {self.last_name} and I am {self._age} years old"

    @classmethod  # get_total_user = classmethod(get_total_users)
    def get_total_users(cls):
        return cls.total

    # def get_age(self):
    #     return self.__age

    # def set_age(self, new_age):
    #     if new_age < 18 or new_age > 90:
    #         raise ValueError("Please enter a valid age")
    #     self.__age = new_age
    #     return True


user1 = User("Jane", "Smith", 20, "jane.smith@gmail.com", "+919876654321")
user2 = User("John", "Doe", 23, "john.doe@gmail.com", "+919854456789")

print(user1)
print(user2)

# print(user1.greet())

# print(User.total)

# print(User.get_total_users())

# print(user2._User__age)

# print(user2._age)

# user2.set_age(309087347890234)
# print(user2.get_age())

# user2.city = "Pune"
# user2.first_name = "ajksd nklasjdnasjkldn"

# user2.age = 8967

# print(user1.city)
# print(user2.city)
# print(user2.first_name)
# print(type(user1))
# print(type(user2))
