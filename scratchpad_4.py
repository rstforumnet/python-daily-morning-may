# # logged_in_user = None

# # if logged_in_user:
# #     print("Welcome to the site")
# # else:
# #     print("Please login to continue")


# age = input("Please enter your age: ")

# if age:
#     age = int(age)
#     if age >= 18 and age < 21:
#         print("You can enter but you cannot drink")
#     elif age >= 21 and age < 65:
#         print("You can enter and you can drink")
#     elif age >= 65:
#         print("Drinks are free")
#     else:
#         print("You are not allowed")
# else:
#     print("You have to type in a number.")


# full_name = "John Doe"

# for char in full_name:
#     print(char, "Hello world")
#     print("------")

# for num in range(5, 10):
#     print(num)


# for num in range(0, 10, 4):
#     print(num)

# for num in range(10, -10, -2):
#     print(num)

# for num in range(1, 21):
#     if num == 5 or num == 16:
#         print("FizzBuzz")
#     elif num % 2 == 0:
#         print(f"{num} - Fizz is even")
#     elif num % 2 == 1:
#         print(f"{num} - Fizz is odd")


secret_password = input("Please tell me the secret password: ")

while secret_password != "helloworld":
    print("Incorrect password")
    secret_password = input("Please enter the secret password again: ")
