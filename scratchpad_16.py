# # # Base Class (Parent Class)
# # class User:
# #     def __init__(self, first_name, last_name, age, email, password):
# #         self.first_name = first_name
# #         self.last_name = last_name
# #         self.age = age
# #         self.email = email
# #         self._password = password

# #     def __repr__(self):
# #         return self.first_name + " " + self.last_name

# #     def login(self, password):
# #         if password == self._password:
# #             print(f"{self.email} just logged in")
# #             return True
# #         else:
# #             raise ValueError("Incorrect Password, please try again")

# #     def logout(self):
# #         print(f"{self.email} is now logged out")
# #         return True

# #     def introduce(self):
# #         return f"Name: {self.first_name} {self.last_name} ({self.age})\nEmail: {self.email}"


# # # Sub Class (Child Class)
# # class Admin(User):
# #     def __repr__(self):
# #         return f"{self.first_name} {self.last_name} (ADMIN)"

# #     def create_group(self, group_name):
# #         return f"{group_name} group created by {self.username}"


# # jane = Admin("Jane", "Smith", 23, "jane.smith@gmail.com", "jane123", "janeworld")

# # print(jane.create_group("Python"))

# # # jane.login("jane123")
# # # print(jane.introduce())
# # # print(jane.create_group("Python"))

# # # john = User("John", "Doe", 20, "john.doe@gmail.com", "john123")

# # # # print(john)
# # # # print(john.introduce())
# # # john.login("john123")


# # class User:
# #     def __init__(self, first_name, last_name, age):
# #         self.first_name = first_name
# #         self.last_name = last_name
# #         self._age = age

# #     def __repr__(self):
# #         return self.first_name + " " + self.last_name

# #     @property
# #     def age(self):
# #         return self._age

# #     @age.setter
# #     def age(self, new_age):
# #         if new_age < 18 or new_age > 80:
# #             raise ValueError("Please enter a valid age")
# #         self._age = new_age
# #         return True

# #     # def get_age(self):
# #     #     return self._age

# #     # def set_age(self, new_age):
# #     #     if new_age < 18 or new_age > 80:
# #     #         raise ValueError("Please enter a valid age")
# #     #     self._age = new_age
# #     #     return True


# # john = User("John", "Doe", 20)

# # john.age = 30
# # print(john.age)

# # # john.set_age(30)
# # print(john.get_age())


# # Base Class (Parent Class)
# class User:
#     def __init__(self, first_name, last_name, age, email, password):
#         self.first_name = first_name
#         self.last_name = last_name
#         self.age = age
#         self.email = email
#         self._password = password

#     def __repr__(self):
#         return self.first_name + " " + self.last_name

#     def login(self, password):
#         if password == self._password:
#             print(f"{self.email} just logged in")
#             return True
#         else:
#             raise ValueError("Incorrect Password, please try again")

#     def logout(self):
#         print(f"{self.email} is now logged out")
#         return True

#     def introduce(self):
#         return f"Name: {self.first_name} {self.last_name} ({self.age})\nEmail: {self.email}"


# # Sub Class (Child Class)
# class Admin(User):
#     def __init__(self, first_name, last_name, age, email, password, address, phone):
#         super().__init__(first_name, last_name, age, email, password)
#         self.address = address
#         self.phone = phone

#     def __repr__(self):
#         return f"{self.first_name} {self.last_name} (ADMIN)"

#     def create_group(self, group_name):
#         return f"{group_name} group created by {self.username}"


# john = Admin(
#     "John", "Doe", 30, "john@gmail.com", "john123", "Some street", "+919876654321"
# )

# print(john.address)
# print(john.phone)


# class Human:
#     def __init__(self, name, age):
#         self.name = name
#         self.age = age

#     def greet(self):
#         return "Hello World"


# class Animal:
#     def __init__(self, name, age):
#         self.name = name
#         self.age = age

#     def greet(self):
#         return "jklbsdfjklbnsadfasdfhbjkl"


# class Mutant(Human, Animal):
#     def greet(self):
#         return 100


# john = Human("John Doe", 20)
# tom = Animal("Tom", 4)
# jack = Mutant("Jack Smith", 40)

# print(john.greet())
# print(jack.greet())

# print(john.greet())
# print(tom.greet())
# print(jack.greet())

# help(jack)


class Human:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __repr__(self):
        return f"{self.name} ({self.age})"

    def greet(self):
        return "Hello World"

    def __len__(self):
        return self.age

    def __add__(self, another_human):
        return Human("New Born", 0)

    def __lt__(self, another_human):
        return self.age < another_human.age


john = Human("John Doe", 30)
jane = Human("Jane Doe", 20)


# print(len(john))

# print(john + jane)
print(john < jane)
