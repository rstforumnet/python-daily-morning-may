# # # def stars(fn):
# # #     def wrapper():
# # #         print("-*-" * 10)
# # #         fn()
# # #         print("-*-" * 10)

# # #     return wrapper


# # # @stars
# # # def say_hello():
# # #     print("Hello World")


# # # # say_hello = stars(say_hello)

# # # say_hello()


# # from functools import wraps


# # def make_upper_case(fn):
# #     """Decorator that uppercases the returned string"""

# #     @wraps(fn)  # wrapper = wraps(fn)(wrapper)
# #     def wrapper(*args, **kwargs):
# #         """Wrapper Function"""
# #         return fn(*args, **kwargs).upper()

# #     return wrapper


# # @make_upper_case  # say_hello = make_upper_case(say_hello)
# # def say_hello(person):
# #     "Function that greets a specific person"
# #     return f"Hello {person}"


# # @make_upper_case
# # def say_hello_again(message, name):
# #     """Function accepts two arguments and greets the user"""
# #     return f"{message}, {name}"


# # # print(say_hello("Jane Doe"))
# # # print(say_hello_again("Hello", "John Doe"))

# # print(say_hello_again.__doc__)
# # print(say_hello.__doc__)
# # print(say_hello_again.__name__)
# # print(say_hello.__name__)


# # file = open("hello.txt")
# # print(file.read())
# # file.close()
# # print(file.closed)

# # with open("hello.txt") as f:
# #     print(f.read())

# # print(f.closed)


# # with open("hello.txt", "a") as file:
# #     file.write("\n")
# #     file.write("-*-" * 10)
# #     file.write("\nHello World\n")
# #     file.write("Hello again\n")
# #     file.write("This is some content\n")
# #     file.write("-*-" * 10)


# # with open("hello.txt", "r+") as file:
# #     file.seek(0)
# #     file.write("THIS IS SOME TEST CONTENT!!!!")

# # with open("hello.txt") as file:
# #     contents = file.read()

# # contents = contents.replace("Hello", "Namaste")

# # with open("hello.txt", "w") as file:
# #     file.write(contents)


# # from pprint import pprint

# # with open("hello.csv") as file:
# #     content = file.read()
# #     rows = content.split("\n")[0:-1]
# #     table = [row.split(",") for row in rows]
# #     pprint(table)


# from csv import reader, DictReader
# from pprint import pprint

# # with open("hello.csv") as file:
# #     csv_reader = reader(file)

# #     pprint(list(csv_reader))

# with open("hello.csv") as file:
#     csv_reader = DictReader(file)

#     pprint(list(csv_reader))

from csv import writer, DictWriter

# with open("hello.csv", "w", newline="") as file:
#     csv_writer = writer(file)
#     csv_writer.writerow(["Full Name", "Email", "Username"])
#     csv_writer.writerow(["John Doe", "john.doe@hey.com", "johndoe123"])
#     csv_writer.writerow(["Jane Doe", "jane.doe@hey.com", "janedoe098"])


with open("hello.csv", "w", newline="") as file:
    headers = ["Full Name", "Username", "Password", "Age"]
    csv_writer = DictWriter(file, fieldnames=headers)
    csv_writer.writeheader()
    csv_writer.writerow(
        {
            "Full Name": "John Doe",
            "Username": "johndoe",
            "Password": "johndoe123",
            "Age": 20,
        }
    )
